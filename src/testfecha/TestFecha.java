package testfecha;

import java.util.Scanner;

public class TestFecha {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int d, m, a;
        String di, me, an;

        //Se pide por teclado el dia, mes y año
        System.out.println("Introduce fecha: ");
        System.out.print("dia: ");
        d = sc.nextInt();
        System.out.print("mes: ");
        m = sc.nextInt();
        System.out.print("año: ");
        a = sc.nextInt();
        System.out.println("Introduce la misma fecha en letras: ");
        System.out.print("dia: ");
        di = sc.next();
        System.out.print("mes: ");
        me = sc.next();
        System.out.print("año: ");
        an = sc.next();
// inicializo clase
        Fecha f1 = new Fecha(d, m, a);
/// mostra resultado 
        System.out.println(" el dia es " + f1.getDia());
        System.out.println(" el mes  es " + f1.getMes());
        System.out.println(" el año es " + f1.getAnio());
        Fecha f2 = new Fecha();
//        System.out.println("\n");
//        System.out.println(" el dia es " + f2.getDia());
//        System.out.println(" el mes  es " + f2.getMes());
//        System.out.println(" el año es " + f2.getAnio());
        System.out.println("\n");

        Fecha f3 = new Fecha(di, me, an);
        System.out.println(" el dia es " + f3.getTexto1().substring(0, 2));
        System.out.println(" el mes  es " + f3.getTexto2().substring(0, 4));
        System.out.println(" el año es " + f3.getTexto3().substring(0,6));
/// concatenar resultar
        System.out.println("\n");
        System.out.println(" el dia es " + f3.getTexto1().substring(0, 4)+" "+ f1.getDia());
        System.out.println(" el mes  es " + f3.getTexto2().substring(0, 4)+" "+f1.getMes());
        System.out.println(" el año es " + f3.getTexto3().substring(0, 4)+" "+f1.getAnio());

    }

}
